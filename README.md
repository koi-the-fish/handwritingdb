## Инструкции для разработчиков проекта

### Поднять проект локально

1)  Скачать [IDE PyCharm](https://www.jetbrains.com/ru-ru/pycharm/download/#section=windows)

2)  Склонировать проект
    ```shell
    git clone https://gitlab.com/koi-the-fish/handwritingdb.git
    ```
3)  Зайти в папку проекта и сконфигурировать его. 
    
    ```shell
    cd handwritingdb
    ```
    PyCharm достаточно умен,
    поэтому сам предложит создать виртуальное окружение и выбрать интерпретатор. 
    Следуйте его инструкциям.
    
4)  Активировать виртуальное окружение

    ```shell
    # windows
    venv\Scripts\activate.bat
    # macOS
    source venv/bin/activate
    ```
5)  Установить необходимые зависимости из файла requirements.txt
    с помощью pip или вручную
    
    ```shell
    pip install requirements.txt
    ```

Но прежде чем запустить проект, нужно поднять базу данных локально:

6)  Скачать MySQL Installer (прим.: на Windows10 не запускался сервер при установке последней версии MySQL 8.0.25,
    поэтому я остановилась на MySQL 8.0.20)
   
7)  На этапе выбора пакета выбрать Custom и установить только MySQL Workbench и MySQL Community Server,
    сконфигурировать и запустить сервер (это установщик подскажет).

В Workbench нам нужен доступ к удаленной базе (прод), которая заполняется на сайте, и к локальной, 
с ней мы будем работать в процессе разработки.

8)  Чтобы подключиться к удаленной базе, создайте подключение со следующими настройками:
    
    ![img.png](readme_imgs/remote_settings.png)
   
    Пароль для hw_admin: hw007

9)  Теперь нужно сделать дамп (копию) базы данных. Для этого на вкладке "Administration" выберите "Export data" 
    и повторите настройки с картинки:
   
    ![img_2.png](readme_imgs/export_dump.png)

10) Чтобы создать локальную копию базы, на локальном подключении создайте новую схему и назовите handwriting_db. 
    Остальные настройки менять не нужно.
   
    ![img_1.png](readme_imgs/create_schema.png)

11) В файле settings.py нашего Django-проекта нужно заменить имя пользователя и пароль на вашего ЛОКАЛЬНОГО юзера. 
    Эти настройки не нужно коммитить гитом, потому что на сервере настройки другие.

   ```
   DB_MYSQL = {
       'default': {
           'ENGINE': 'django.db.backends.mysql',
           'NAME': 'handwriting_db',
           'USER': 'root ИЛИ ВАШ ЮЗЕР',
           'PASSWORD': 'ВАШ ПАРОЛЬ',
           'HOST': 'localhost',
       }
   }
   ```

12) Теперь, наконец, можно запустить проект:

    ```
    #windows
    py manage.py runserver
    #macos
    python manage.py runserver
    ```

Вы великолепны!

### Обновление сайта с базой:

Адрес сайта: http://188.254.76.84:8000/admin/

Пользователь: django_admin

Пароль: django007

#### Изменения локально
1. pull на локали
   ```shell script
   git pull
   ```
2. Активировать venv
   ```shell script
    source venv/bin/activate
    ```
3. Сделать необходимые изменения
3. Создать миграции локально, если необходимо (если изменялись поля модели или мета)
    ```shell script
    ./manage.py makemigrations
    ```
4. push в мастер

#### Изменения на сервере
5. pull на сервере **(Webmin)**
6. Активировать venv (SSH client)
    ```shell script
    source venv/bin/activate
    ```
8. применить миграции на сервере (SSH client)
    ```shell script
    ./manage.py migrate
    ```
7. Перезапустить gunicorn (Webmin) 
    ```shell script
    sudo systemctl restart gunicorn 
    ```

####Запуск MySQL
Сделать дамп
```shell script
mysqldump -u root -p handwritingdb > /Users/n.ogoreltseva/handwritingdb/nata_25_04.sql

```
Выпустить дамп
```shell script
mysql -uroot -p handwritingdb < /Users/n.ogoreltseva/handwritingdb/handwriting_db_05_28.sql
```
Запустить MySQL
```shell script
mysql -u root -p
```
## Полезные штуки

Запустить команду из /main/management/commands

```shell
./manage.py command_name
```

###Reorder-admin package
 
Чтобы располагать модели в админке в нужном нам порядке, мы используем пакет 
[django-admin-reorder](https://django-modeladmin-reorder.readthedocs.io/en/latest/readme.html)
Он уже настроен в проекте, нужно только установить пакет через pip.

### Django-debug-toolbar

Для отладки работы джанго приложения мы используем пакет 
[django-debug-toolbar](https://django-debug-toolbar.readthedocs.io/en/latest/index.html).
Его нужно установить и настроить самостоятельно.

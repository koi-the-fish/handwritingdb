from django.contrib import admin

# Register your models here.
from . import models

admin.site.register(models.HwConcretization)
admin.site.register(models.HwElement)
admin.site.register(models.HwGeneralSignType)
admin.site.register(models.HwGeneralSignValue)
admin.site.register(models.HwGeneralWriting)
admin.site.register(models.HwImage)
admin.site.register(models.HwLetter)
admin.site.register(models.HwLetterConcretization)
admin.site.register(models.HwLetterElement)
admin.site.register(models.HwLinkNext)
admin.site.register(models.HwPartialSign)
admin.site.register(models.HwPartialSignType)
admin.site.register(models.HwPartialSignValue)
admin.site.register(models.HwPartialSignValueConcretization)
admin.site.register(models.HwPartialWriting)
admin.site.register(models.HwSet)
admin.site.register(models.HwTranscription)
admin.site.register(models.HwType)
admin.site.register(models.Person)
admin.site.register(models.Position)
admin.site.register(models.QAnswer)
# @admin.register(models.QAnswer)
# class QAnswerAdmin(admin.ModelAdmin):
#     list_display = ('question', 'number_in_question', 'answer_text')
#     list_filter = ('question')
admin.site.register(models.QAnswerText)
admin.site.register(models.QComment)
admin.site.register(models.QIntervalName)
admin.site.register(models.QIntervalScore)
admin.site.register(models.QIntervalSet)
admin.site.register(models.QIntervalSetName)
admin.site.register(models.QPartitioning)
admin.site.register(models.QPartitioningScale)
admin.site.register(models.QPartitioningScaleName)
@admin.register(models.QPersonSelection)
class QPersonSelectionAdmin(admin.ModelAdmin):
    list_display = ('person', 'answer')
    list_filter = ('person',)


@admin.register(models.QQuestion)
class QQuestionAdmin(admin.ModelAdmin):
    list_display = ('number_in_questionnaire', 'questionnaire', 'question_text',)
    list_filter = ('questionnaire',)
    # list_editable = ('questionnaire', 'question_text',)
    list_select_related = ('questionnaire', 'question_text',)


@admin.register(models.QQuestionnaire)
class QQuestionnaireAdmin(admin.ModelAdmin):
    list_display = ('questionnaire_nickname', 'questionnaire_description')


@admin.register(models.QQuestionScale)
class QQuestionScale(admin.ModelAdmin):
    list_display = ('scale', 'question',)
    list_filter = ('scale',)


admin.site.register(models.QQuestionText)
admin.site.register(models.QResult)


@admin.register(models.QScale)
class QScaleAdmin(admin.ModelAdmin):
    list_display = ('scale_name', 'scale_nickname')







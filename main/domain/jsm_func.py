
# разделить примеры на + и -
def divide_examples(examples, split_func, *args, **div_params):
    examples_plus = []
    examples_minus = []

    for ex in examples:
        if split_func(ex, *args, **div_params):
            examples_plus.append(ex)
        else:
            examples_minus.append(ex)

    return examples_plus, examples_minus


def norris(examples, flag=True, extended=False):
    result_list = []
    for k in range(len(examples)):
        if extended:
            include_trans(k, examples, result_list)
        else:
            include(k, examples, result_list, flag)
    return result_list


def jsm_create_similarities(examples, method=norris, **kwargs):
    return method(examples, **kwargs)


def include(k, examples, result_list, flag):
    # intensional -- множество признаков, которыми обладают все объекты данного множества
    # extensional -- множество объектов, которые обладают всеми признаками из данного множества
    # optional -- ????
    # цифры обозначают позицию в списке: екст на 0 месте, инт на 1 и тд
    extensional = 0
    intensional = 1
    optional = 2
    optional_2 = 3

    for i in range(len(result_list)):
        if result_list[i][intensional] <= examples[k][intensional - 1]:
            result_list[i][extensional] |= {k}
            if flag:
                result_list[i][optional] |= {examples[k][optional - 1]}
            else:
                result_list[i][optional] |= examples[k][optional - 1]
                result_list[i][optional_2] |= examples[k][optional_2 - 1]
        else:
            z = result_list[i][intensional] & examples[k][intensional - 1]
            if {m for m in range(k) if
                m not in result_list[i][extensional] and z <= examples[m][
                    intensional - 1]} == set():
                if flag:
                    result_list.append([
                        result_list[i][extensional]
                        | {k}, z, result_list[i][optional]
                        | {examples[k][optional - 1]}
                    ])
                else:
                    result_list.append([
                        result_list[i][extensional]
                        | {k}, z, result_list[i][optional]
                        | examples[k][optional - 1],
                        result_list[i][optional_2]
                        | examples[k][optional_2 - 1]
                    ])
    # example: [ [['1;1', '2;1'], 'pers1'], [['1;1', '2;1'], 'pers2'] ]
    if {m for m in range(k) if examples[k][intensional - 1] <= examples[m][intensional - 1]} == set():
        if flag:
            result_list.append([{k}, examples[k][intensional - 1],
                                {examples[k][optional - 1]}])
        else:
            result_list.append(
                [{k}, examples[k][intensional - 1], examples[k][optional - 1],
                 examples[k][optional_2 - 1]])


def include_trans(k, examples, result_list):
    extensional = 0
    intensional = 1
    intensional_2 = 2
    person = 3

    for i in range(len(result_list)):
        if result_list[i][intensional] <= result_list[k][0]:
            result_list[i][extensional] |= {k}
            result_list[i][person] |= {result_list[k][2]}
            # L[i][scale] |= {O[k][3]}
        else:
            z1 = result_list[i][intensional] & examples[k][0]
            z2 = result_list[i][intensional_2] & examples[k][1]
            if {m for m in range(k) if
                m not in result_list[i][extensional] and z1 <= examples[m][
                    0]} == set():
                result_list.append([result_list[i][extensional] | {k}, z1, z2,
                                    result_list[i][person] | {examples[k][2]}])

    if {m for m in range(k) if examples[k][0] <= examples[m][0]} == set():
        result_list.append(
            [{k}, examples[k][0], examples[k][1], {examples[k][2]}])


def sort_hypotheses(h_list, person_index=1):
    # 1. Сортируем по количеству респондентов по убыванию
    h_list.sort(key=lambda i: len(i[person_index]), reverse=True)

    final_sorted_h_list = []
    sorted_h_list = []
    # 2. Пока не исчерпали исходный список.
    # Берём первый элемент, ищем его наибольшее подмножество в исходном списке (удаляя из исходного)
    while h_list:
        sorted_h_list.append(h_list.pop(0))
        for s in sorted_h_list:
            for i in range(len(h_list)):
                if i < len(h_list):
                    if h_list[i][person_index] <= s[person_index]:
                        sorted_h_list.append(h_list.pop(i))
                        break
        final_sorted_h_list.extend(sorted_h_list)
        sorted_h_list.clear()
    return final_sorted_h_list


# sort + biggest hypothesis
def sort_hypotheses_2(h_list, person_index=1):
    # 1. Сортируем по количеству респондентов по убыванию
    h_list.sort(key=lambda i: len(i[person_index]), reverse=True)

    biggest_hypothesis = []
    final_sorted_h_list = []
    sorted_h_list = []
    # 2. Пока не исчерпали исходный список.
    # Берём первый элемент, ищем его наибольшее подмножество в исходном списке (удаляя из исходного)
    while h_list:
        h = h_list.pop(0)
        sorted_h_list.append(h)
        biggest_hypothesis.append(h)
        for s in sorted_h_list:
            for i in range(len(h_list)):
                if i < len(h_list):
                    if h_list[i][person_index] <= s[person_index]:
                        sorted_h_list.append(h_list.pop(i))
                        break
        final_sorted_h_list.extend(sorted_h_list)
        sorted_h_list.clear()
    return final_sorted_h_list, biggest_hypothesis


def jsm_filtration(candidates_of_possible_causes, ext_minimum=2,
                   int_minimum=2, intensional=1, optional=2, extended=False):
    # candidates_of_possible_causes - Candidates of Possible Causes
    # ext_minimum - порог по мощности (количеству элементов в множестве) для экстенсионала
    # int_minimum - порог по мощности (количеству элементов в множестве) для интенсионала
    extensional = 0
    # intensional = 1
    # person = 2
    # scale = 3
    optional_2 = optional + 1

    result_list = []
    for i in range(len(candidates_of_possible_causes)):
        if len(candidates_of_possible_causes[i][
                   extensional]) >= ext_minimum and len(
                candidates_of_possible_causes[i][intensional]) >= int_minimum:
            if extended:
                if candidates_of_possible_causes[i][optional] != set() and \
                        candidates_of_possible_causes[i][optional_2] != set():
                    result_list.append(
                        [candidates_of_possible_causes[i][intensional],
                         candidates_of_possible_causes[i][optional],
                         candidates_of_possible_causes[i][optional_2]])
            else:
                result_list.append(
                    [candidates_of_possible_causes[i][intensional],
                     candidates_of_possible_causes[i][optional]])
            # L.append([candidates_of_possible_causes[i][Int], candidates_of_possible_causes[i][person], candidates_of_possible_causes[i][scale]])
    return result_list


def jsm_filtration_trans(candidates_of_possible_causes, ext_minimum=2,
                         int_minimum=1):
    extensional = 0
    intensional = 1
    intensional_2 = 2
    person = 3

    result_list = []
    for i in range(len(candidates_of_possible_causes)):
        if len(candidates_of_possible_causes[i][
                   extensional]) >= ext_minimum and len(
                candidates_of_possible_causes[i][intensional]) >= int_minimum:
            result_list.append([candidates_of_possible_causes[i][intensional],
                                candidates_of_possible_causes[i][intensional_2],
                                candidates_of_possible_causes[i][person]])
    return result_list


def include_old(k, examples, result_list):
    # intensional -- множество признаков, которыми обладают все объекты данного множества
    # extensional -- множество объектов, которые обладают всеми признаками из данного множества
    # extensional, intensional, person -- номера позиций в списке
    extensional = 0
    intensional = 1
    person = 2

    for i in range(len(result_list)):
        if result_list[i][intensional] <= examples[k][0]:
            result_list[i][extensional] |= {k}
            result_list[i][person] |= {examples[k][1]}
            # L[i][scale] |= {O[k][3]}
        else:
            z = result_list[i][intensional] & examples[k][0]
            if {m for m in range(k) if
                m not in result_list[i][extensional] and z <= examples[m][
                    0]} == set():
                result_list.append([result_list[i][extensional] | {k}, z,
                                    result_list[i][person] | {examples[k][1]}])

    if {m for m in range(k) if examples[k][0] <= examples[m][0]} == set():
        result_list.append([{k}, examples[k][0], {examples[k][1]}])
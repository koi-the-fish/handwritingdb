from django.core.management.base import BaseCommand

from main.models import QQuestion, QScale, QQuestionScale


# ОСТ
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         questions = QQuestion.objects.filter(questionnaire_id=1)
#         for question in questions:
#             num = question.number_in_questionnaire
#             obj = QQuestionScale.objects.create(
#                     question_id=question.question_id,
#                     scale_id=1
#                 )
#             if num in [11, 30, 57, 62, 67, 78, 86, 3, 34, 74, 90, 105]:
#                 obj.scale_id = 2
#             elif num in [20, 25, 35, 38, 47, 66, 71, 76, 101, 104, 54, 59]:
#                 obj.scale_id = 3
#             elif num in [2, 9, 18, 26, 45, 68, 85, 99, 31, 81, 87, 93]:
#                 obj.scale_id = 4
#             elif num in [1, 13, 19, 33, 46, 49, 55, 77, 29, 43, 70, 94]:
#                 obj.scale_id = 5
#             elif num in [24, 37, 39, 51, 72, 92, 5, 10, 16, 56, 96, 102]:
#                 obj.scale_id = 6
#             elif num in [14, 17, 28, 40, 60, 61, 69, 79, 88, 91, 95, 97]:
#                 obj.scale_id = 7
#             elif num in [6, 7, 21, 36, 41, 48, 53, 63, 75, 80, 84, 100]:
#                 obj.scale_id = 25
#             elif num in [32, 52, 89, 12, 23, 44, 65, 73, 82]:
#                 obj.scale_id = 24
#             obj.save()

# ОЧХ
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         questions = QQuestion.objects.filter(questionnaire_id=2)
#         for question in questions:
#             num = question.number_in_questionnaire
#             obj = QQuestionScale.objects.create(
#                     question_id=question.question_id,
#                     scale_id=8
#                 )
#             if num in [7, 29, 41, 44, 60, 68, 80]:
#                 obj.scale_id = 9
#             elif num in [4, 14, 17, 26, 39, 58, 65, 75]:
#                 obj.scale_id = 10
#             elif num in [8, 20, 30, 42, 51, 61, 69, 78]:
#                 obj.scale_id = 11
#             elif num in [1, 11, 23, 33, 45, 54, 63, 72]:
#                 obj.scale_id = 12
#             elif num in [9, 21, 43, 70, 79, 31, 52, 62]:
#                 obj.scale_id = 13
#             elif num in [6, 18, 28, 40, 49, 59, 67, 77]:
#                 obj.scale_id = 14
#             elif num in [10, 19, 22, 32, 34, 36, 53, 71]:
#                 obj.scale_id = 15
#             elif num in [3, 13, 35, 47, 55, 64, 74, 25]:
#                 obj.scale_id = 16
#             elif num in [16, 27, 38, 48, 57, 66, 76, 5]:
#                 obj.scale_id = 17
#             obj.save()

# САН
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         questions = QQuestion.objects.filter(questionnaire_id=3)
#         for question in questions:
#             num = question.number_in_questionnaire
#             obj = QQuestionScale.objects.create(
#                     question_id=question.question_id,
#                     scale_id=18
#                 )
#             if num in [3, 4, 9, 10, 15, 16, 21, 22, 27, 28]:
#                 obj.scale_id = 19
#             elif num in [5, 6, 11, 12, 17, 18, 23, 24, 29, 30]:
#                 obj.scale_id = 20
#             obj.save()


# БП
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         questions = QQuestion.objects.filter(questionnaire_id=4)
#         for question in questions:
#             num = question.number_in_questionnaire
#             obj = QQuestionScale.objects.create(
#                     question_id=question.question_id,
#                     scale_id=21
#                 )
#             if num in [10, 11, 12, 13, 14, 15, 16]:
#                 obj.scale_id = 22
#             elif num in [17, 18, 19, 20, 21, 22, 23, 24]:
#                 obj.scale_id = 23
#             obj.save()


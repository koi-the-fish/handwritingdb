from django.core.management.base import BaseCommand

from main.models import QAnswer, QQuestion, QAnswerText

# Опросник: БП
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         q_questions = QQuestion.objects.filter(questionnaire_id=4)
#         for q_question in q_questions:
#             for num, answer_text in enumerate([3, 4, 7, 5, 6], start=1):
#                 QAnswer.objects.create(
#                     question=q_question,
#                     number_in_question=num,
#                     answer_text=answer_text,
#                     score=num
#                 )

# Опросник: ОЧХ
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         q_questions = QQuestion.objects.filter(questionnaire_id=2)
#         for q_question in q_questions:
#             for num, answer_text in enumerate([3, 4, 5, 6], start=1):
#                 QAnswer.objects.create(
#                     question=q_question,
#                     number_in_question=num,
#                     answer_text=QAnswerText.objects.get(answer_text_id=answer_text),
#                     score=num
#                 )


# ОЧХ обратная оценка для 5, 25, 31, 46, 50, 52, 62
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         for question_id in [118, 142, 149, 174, 178, 180, 190]:
#             for score in [1, 2, 3, 4]:
#                 obj = QAnswer.objects.get(question_id=question_id, number_in_question=score)
#                 obj.score = 5-score
#                 obj.save()


# ОСТ не работает цикл
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         q_questions = QQuestion.objects.filter(questionnaire_id=1)
#         for q_question in q_questions:
#             for num, answer_text, score in [(1, 1, 1), (2, 2, 0)]:
#                 if q_question in [4, 8, 15, 22, 42, 50, 58, 64, 96, 11, 30, 57, 62, 67, 78, 86, 20, 25, 35, 38, 47, 66,
#                                   71, 76, 101, 104, 2, 9, 18, 26, 45, 68, 85, 99, 1, 13, 19, 33, 46, 49, 55, 77, 24, 37,
#                                   39, 51, 72, 92, 14, 17, 28, 40, 60, 61, 69, 79, 88, 91, 95, 97, 6, 7, 21, 36, 41, 48,
#                                   53, 63, 75, 80, 84, 100, 32, 52, 89]:
#                     QAnswer.objects.create(
#                         question=q_question,
#                         number_in_question=num,
#                         answer_text=QAnswerText.objects.get(answer_text_id=answer_text),
#                         score=score
#                     )
#                 else:
#                     QAnswer.objects.create(
#                         question=q_question,
#                         number_in_question=num,
#                         answer_text=QAnswerText.objects.get(answer_text_id=answer_text),
#                         score=1-score
#                     )

# ОСТ исправила баллы
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         for question_id in [1,2,4,6,7,8,9,11,13,14,16,19,20,21,22,23,24,26,27,30,32,35,
#                             36,38,39,40,41,42,43,44,45,48,49,50,51,52,53,55,56,57,60,62,
#                             63,65,66,67,68,69,71,72,73,74,76,77,80,81,82,83,84,86,92,93,
#                             94,96,97,99,100,103,104,105, 107, 108,109,112, 263]:
#             obj = QAnswer.objects.get(question_id=question_id, answer_text_id=2)
#             obj.score = 0
#             obj.save()


# САН
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         q_questions = QQuestion.objects.filter(questionnaire_id=3)
#         for q_question in q_questions:
#             for num, answer_text in enumerate([11, 10, 9, 8, 9, 10, 11], start=1):
#                 QAnswer.objects.create(
#                     question=q_question,
#                     number_in_question=num,
#                     answer_text=QAnswerText.objects.get(answer_text_id=answer_text),
#                     score=num
#                 )

# САН обратная шкала
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         for question_id in [211,212,215,216,217,218,221,222,224,225,228,229,230,231,234,235,236,237,268,269]:
#             for score in [7, 6, 5, 4, 3, 2, 1]:
#                 obj = QAnswer.objects.get(question_id=question_id, number_in_question=score)
#                 obj.score = 8-score
#                 obj.save()

# Generated by Django 3.0.2 on 2020-05-02 18:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0011_auto_20200502_1422'),
    ]

    operations = [
        migrations.AddField(
            model_name='qanswer',
            name='question_id_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING,
                                    to='main.QQuestion'),
        ),
        migrations.RemoveField(
            model_name='qanswer',
            name='question_id_id',
        ),
    ]

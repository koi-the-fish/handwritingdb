from django.core.cache import cache
from django.db.models.signals import post_save
from django.dispatch import receiver

from main.models import QResult, HwSet


@receiver(post_save, sender=QResult)
@receiver(post_save, sender=HwSet)
def invalidate_cache(sender, **kwargs):
    cache.clear()
    print("cache is empty!")

# Нахождение потенциальных гипотез
def find_potential_hypothesis(questionnaire_id=1, partitioning_scale_name_id=2, resps_minimum=2, div_param=1, sim_signs='gen_elem'):
    # 0. находим пересечение респондентов по опросникам и подписям

    sc = QQuestionScale.objects.filter(question__questionnaire=questionnaire_id).values_list('scale_id',
                                                                                             flat=True).exclude(
        scale_id=24)
    scales = QScale.objects.filter(scale_id__in=sc)
    respondents_q = QResult.objects.filter(scale_id__in=scales).values_list('person_id', flat=True).distinct()
    # respondents_q = QResult.objects.filter(scale_id__in=scales).distinct()
    respondents_hw = HwGeneralWriting.objects.all().values_list('set__person', flat=True).distinct()
    # respondents_hw = HwGeneralWriting.objects.all().distinct()
    resp_intersec_plus = []
    resp_intersec_minus = []
    # for resp in Person.objects.all().values_list('person_id', flat=True).distinct():
    results_plus = QResult.objects.filter(scale_id=11, result_score__lte=3).values_list('person_id', flat=True)
    results_minus = QResult.objects.filter(scale_id=11, result_score__gte=4).values_list('person_id', flat=True)
    # print(results_plus)
    # print(results_minus)
    for resp in Person.objects.all().values_list('person_id', flat=True):
        if resp in respondents_q and resp in respondents_hw:
            if int(div_param) == 1:
                div_plus = 'женщины'
                div_minus = 'мужчины'
                if Person.objects.get(pk=resp).sex == 'ж':
                    resp_intersec_plus.append(Person.objects.get(pk=resp).person_id)
                elif Person.objects.get(pk=resp).sex == 'м':
                    resp_intersec_minus.append(Person.objects.get(pk=resp).person_id)
            elif int(div_param) == 2:
                div_plus = 'возбудимость низкая (<4 стенов)'
                div_minus = 'возбудимость средняя (>3 стенов)'
                if Person.objects.get(pk=resp).person_id in results_plus:
                    resp_intersec_plus.append(Person.objects.get(pk=resp).person_id)
                elif Person.objects.get(pk=resp).person_id in results_minus:
                    resp_intersec_minus.append(Person.objects.get(pk=resp).person_id)

    # resp_intersec.append(Person.objects.get(pk=resp).person_id)

    # 1. Сходство по общим признакам подписей
    if sim_signs == 'gen_elem':
        filtered_cpcplus_hw, filtered_cpcminus_hw = find_similarities_gw_potential(resp_intersec_plus, resp_intersec_minus)
    else:
        filtered_cpcplus_hw, filtered_cpcminus_hw = find_similarities_sign_groups(resp_intersec_plus, resp_intersec_minus)
    # наибольшие
    sorted_hw_plus, biggest_hypothesis_hw_plus = sort_hypothesis_2(filtered_cpcplus_hw)
    sorted_hw_minus, biggest_hypothesis_hw_minus = sort_hypothesis_2(filtered_cpcminus_hw)

    # 2. Сходства по опросникам

    filtered_cpcplus_q, filtered_cpcminus_q = find_similarities_q(resp_intersec_plus, resp_intersec_minus,
                                                                  resps_minimum, questionnaire_id,
                                                                  partitioning_scale_name_id)
    sorted_q_plus, biggest_hypothesis_q_plus = sort_hypothesis_2(filtered_cpcplus_q)
    sorted_q_minus, biggest_hypothesis_q_minus = sort_hypothesis_2(filtered_cpcminus_q)

    # добавляем по пустому множеству к каждому виду гипотез
    sorted_hypothesis_hw_plus_new = []
    sorted_hypothesis_hw_minus_new = []

    for h in sorted_hw_plus:
        sorted_hypothesis_hw_plus_new.append([h[1], set(), h[0]])
    for h in sorted_hw_minus:
        sorted_hypothesis_hw_minus_new.append([h[1], set(), h[0]])

    sorted_hypothesis_q_plus_new = []
    sorted_hypothesis_q_minus_new = []

    for h in sorted_q_plus:
        sorted_hypothesis_q_plus_new.append([h[1], h[0], set()])
    for h in sorted_q_minus:
        sorted_hypothesis_q_minus_new.append([h[1], h[0], set()])

    # 3. Пересекаем
    # объединяем гипотезы
    all_hypothesis_plus = sorted_hypothesis_hw_plus_new + sorted_hypothesis_q_plus_new
    sim_all_plus = jsm_create_similarities(all_hypothesis_plus, flag=False)
    filtered_sim_all_plus = jsm_filtration(sim_all_plus, int_minimum=resps_minimum, extended=True)
    sorted_sim_all_plus = sort_hypothesis(filtered_sim_all_plus, person_index=0)

    all_hypothesis_minus = sorted_hypothesis_hw_minus_new + sorted_hypothesis_q_minus_new
    sim_all_minus = jsm_create_similarities(all_hypothesis_minus, flag=False)
    filtered_sim_all_minus = jsm_filtration(sim_all_minus, int_minimum=resps_minimum, extended=True)
    sorted_sim_all_minus = sort_hypothesis(filtered_sim_all_minus, person_index=0)
    return sorted_sim_all_plus, sorted_sim_all_minus, sorted_hypothesis_hw_plus_new, sorted_hypothesis_hw_minus_new, \
           sorted_hypothesis_q_plus_new, sorted_hypothesis_q_minus_new, div_plus, div_minus


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def potential_hypotheses_view(request):
    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()
    questionnaire_id = request.GET.get('select_questionnaire', 1)
    partitioning_scale_name_id = request.GET.get('select_partitioning', 2)
    resps_minimum = int(request.GET.get('resps_minimum', 2))

    div_param = request.GET.get('div_param', 1)
    sim_signs = request.GET.get('sim_signs', 'groups')

    sorted_sim_all_plus, sorted_sim_all_minus, sorted_hypothesis_hw_plus_new, sorted_hypothesis_hw_minus_new, \
        sorted_hypothesis_q_plus_new, sorted_hypothesis_q_minus_new, div_plus, div_minus = find_potential_hypothesis(
            questionnaire_id=questionnaire_id, partitioning_scale_name_id=partitioning_scale_name_id,
            resps_minimum=resps_minimum, sim_signs=sim_signs, div_param=div_param)
    quantity_plus = len(sorted_sim_all_plus)
    quantity_minus = len(sorted_sim_all_minus)
    return render(request, 'research/potential_hypotheses.html', {'potential_hypothesis_plus': sorted_sim_all_plus,
                                                                  'potential_hypothesis_minus': sorted_sim_all_minus,
                                                                  'questionnaires': questionnaires,
                                                                  'partitionings': partitionings,
                                                                  'questionnaire_id': int(questionnaire_id),
                                                                  'partitioning_scale_name_id': int(
                                                                      partitioning_scale_name_id),
                                                                  'resps_minimum': resps_minimum,
                                                                  'title': 'Потенциальные гипотезы',
                                                                  'quantity_plus': quantity_plus,
                                                                  'quantity_minus': quantity_minus,
                                                                  'div_param': int(div_param),
                                                                  'is_gen_elem': True if sim_signs == 'gen_elem' else False,
                                                                  'div_plus': div_plus,
                                                                  'div_minus': div_minus
                                                                  })


# функция разделения примеров на + и - по полу
def divide_by_result(example, res_plus, res_minus):
    if example[1].person_id in res_plus:
        return True
    elif example[1].person_id in res_minus:
        return False


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin's, 'expert'])
def jsm_gw(questionnaire_id, sim_signs):

    sc = QQuestionScale.objects.filter(question__questionnaire=questionnaire_id).values_list('scale_id',
                                                                                             flat=True).exclude(
        scale_id=24)
    scales = QScale.objects.filter(scale_id__in=sc)
    respondents_q = QResult.objects.filter(scale_id__in=scales).values_list('person_id', flat=True).distinct()

    respondents_hw_plus = HwGeneralWriting.objects.filter(set__person__in=respondents_q,
                                                          set__person__sex='ж').values_list('set__person',
                                                                                            flat=True).distinct()
    respondents_hw_minus = HwGeneralWriting.objects.filter(set__person__in=respondents_q,
                                                           set__person__sex='м').values_list('set__person',
                                                                                             flat=True).distinct()

    if sim_signs == 'groups':
        filtered_sim_plus, filtered_sim_minus = find_similarities_sign_groups(respondents_hw_plus, respondents_hw_minus)
    else:
        filtered_sim_plus, filtered_sim_minus = find_similarities_gw_potential(respondents_hw_plus, respondents_hw_minus)

    # формируем список с объектами вместо номеров
    ocplus = []
    for f in filtered_sim_plus:
        o = set()
        obj_gen = HwGeneralSignValue.objects.filter(jsm_number__in=f[0]).order_by(
            'general_sign_type__general_sign_type_number')
        obj_part = HwPartialSign.objects.filter(general_jsm_number__in=f[0])
        for og in obj_gen:
            o.add(og)

        seen = set()
        for op in obj_part:
            if op.general_jsm_number not in seen:
                o.add(op)
                seen.add(op.general_jsm_number)

        ocplus.append([o, f[1]])

    ocminus = []
    for f in filtered_sim_minus:
        o = set()
        obj_gen = HwGeneralSignValue.objects.filter(jsm_number__in=f[0]).order_by(
            'general_sign_type__general_sign_type_number')
        obj_part = HwPartialSign.objects.filter(general_jsm_number__in=f[0])
        for og in obj_gen:
            o.add(og)

        seen = set()
        for op in obj_part:
            if op.general_jsm_number not in seen:
                o.add(op)
                seen.add(op.general_jsm_number)

        ocminus.append([o, f[1]])

    sorted_ocplus = sort_hypothesis(filtered_sim_plus)
    sorted_ocminus = sort_hypothesis(filtered_sim_minus)

    #quantity_plus = len(sorted_ocplus)
    #quantity_minus = len(sorted_ocminus)
    # print(len(sorted_ocplus))

    return sorted_ocplus, sorted_ocminus
    # return render(request, 'research/similarities_gw.html', {'sim_plus': sorted_ocplus,
    #                                                 'sim_minus': sorted_minus,
    #                                                 'title': 'Сходства по общим и обобщенным частным признакам',
    #                                                 'quantity_plus': quantity_plus,
    #                                                 'quantity_minus': quantity_minus,
    #                                                 })


def find_similarities_gw(resp_list_plus, resp_list_minus):
    oplus = []
    for s in HwSet.objects.filter(verity=1, person__person_id__in=resp_list_plus).order_by(
            'person__person_id'):
        gw = HwGeneralWriting.objects.filter(set=s)
        pw = HwPartialWriting.objects.filter(first_letter_transcription__set=s)
        gv = set()

        # общие признаки подписи
        for g in gw:
            feature = g.general_sign_value.jsm_number
            if feature not in ['1;1', '2;2', '9;1', '11;1', '12;1'] and feature:
                # if feature in ['5;2', '5;3']:
                #     gv.add('5;2')
                # elif feature in ['3;1', '3;3']:
                #     gv.add('3;1')
                # elif feature in ['4;1', '4;3']:
                #     gv.add('4;1')
                # else:
                gv.add(feature)
                # print('h2: ', g.general_sign_value.jsm_number)

        # обобщенные частные признаки подписи
        for p in pw:
            if p.partial_sign.general_jsm_number:
                if p.partial_sign.partial_sign_value.partial_sign_type.partial_sign_type_number == 1:
                    gv.add(
                        p.partial_sign.partial_sign_value.partial_sign_type.partial_sign_type_number + ';' +
                        p.partial_sign.partial_sign_value.partial_sign_value_number + ';')
                else:
                    gv.add(p.partial_sign.general_jsm_number)
        # for p in pw:
        # if p.partial_sign.opt_general_jsm_number:
        # gv.add(p.partial_sign.opt_general_jsm_number)
        # print(p.partial_sign.opt_general_jsm_number)
        oplus.append([gv, s.person])

    ominus = []
    for s in HwSet.objects.filter(verity=1, person__person_id__in=resp_list_minus).order_by(
            'person__person_id'):
        gw = HwGeneralWriting.objects.filter(set=s)
        pw = HwPartialWriting.objects.filter(first_letter_transcription__set=s)
        gv = set()

        # общие признаки подписи
        for g in gw:
            feature = g.general_sign_value.jsm_number
            if feature not in ['1;1', '2;2', '9;1', '11;1', '12;1'] and feature:
                # if feature in ['5;2', '5;3']:
                #     gv.add('5;2')
                # elif feature in ['3;1', '3;3']:
                #     gv.add('3;1')
                # elif feature in ['4;1', '4;3']:
                #     gv.add('4;1')
                # else:
                gv.add(feature)
                # print('h2: ', g.general_sign_value.jsm_number)

        # обобщенные частные признаки подписи
        for p in pw:
            if p.partial_sign.general_jsm_number:
                if p.partial_sign.partial_sign_value.partial_sign_type.partial_sign_type_number == 1:
                    gv.add(
                        p.partial_sign.partial_sign_value.partial_sign_type.partial_sign_type_number + ';' +
                        p.partial_sign.partial_sign_value.partial_sign_value_number + ';')
                else:
                    gv.add(p.partial_sign.general_jsm_number)
        # for p in pw:
        # if p.partial_sign.opt_general_jsm_number:
        # gv.add(p.partial_sign.opt_general_jsm_number)
        # print(p.partial_sign.opt_general_jsm_number)
        ominus.append([gv, s.person])

    # формируем список [[{мн-во неинф призн для человека}, чел], [], ...     ]

    # находим сходства
    cpcplus = jsm_create_similarities(oplus)
    cpcminus = jsm_create_similarities(ominus)
    # Фильтруем [ [{'',''},{pp}], [] ]
    filtered_cpcplus = jsm_filtration(cpcplus)
    filtered_cpcminus = jsm_filtration(cpcminus)
    # формируем список с объектами вместо номеров
    # ocplus = []
    # for f in filtered_cpcplus:
    #     obj = HwGeneralSignValue.objects.filter(jsm_number__in=f[0]).order_by(
    #         'general_sign_type__general_sign_type_number')
    #     ocplus.append([obj, f[1]])
    return filtered_cpcplus, filtered_cpcminus


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def similarities_general_view(request):

    # handles

    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()
    questionnaire_id = request.GET.get('select_questionnaire', 1)
    partitioning_scale_name_id = request.GET.get('select_partitioning', 2)
    resps_minimum = int(request.GET.get('resps_minimum', 2))
    div_param = request.GET.get('div_param', 1)

    sim_signs = request.GET.get('sim_signs', 'groups')

    # 0. находим пересечение респондентов по опросникам и подписям
    sc = QQuestionScale.objects.filter(question__questionnaire=questionnaire_id).values_list('scale_id',
                                                                                             flat=True).exclude(
        scale_id=24)
    scales = QScale.objects.filter(scale_id__in=sc)
    respondents_q = QResult.objects.filter(scale_id__in=scales).values_list('person_id', flat=True).distinct()
    respondents_hw = HwGeneralWriting.objects.all().values_list('set__person', flat=True).distinct()

    resp_intersec_plus = []
    resp_intersec_minus = []

    results_plus = QResult.objects.filter(scale_id=11, result_score__lte=3).values_list('person_id', flat=True)
    results_minus = QResult.objects.filter(scale_id=11, result_score__gte=4).values_list('person_id', flat=True)

    for resp in Person.objects.all().values_list('person_id', flat=True):
        if resp in respondents_q and resp in respondents_hw:
            if int(div_param) == 1:
                div_plus = 'женский пол'
                div_minus = 'мужской пол'
                if Person.objects.get(pk=resp).sex == 'ж':
                    resp_intersec_plus.append(Person.objects.get(pk=resp).person_id)
                elif Person.objects.get(pk=resp).sex == 'м':
                    resp_intersec_minus.append(Person.objects.get(pk=resp).person_id)
            elif int(div_param) == 2:
                div_plus = 'возбудимость низкая (<4 стенов)'
                div_minus = 'возбудимость средняя (>3 стенов)'
                if Person.objects.get(pk=resp).person_id in results_plus:
                    resp_intersec_plus.append(Person.objects.get(pk=resp).person_id)
                elif Person.objects.get(pk=resp).person_id in results_minus:
                    resp_intersec_minus.append(Person.objects.get(pk=resp).person_id)
    #

    # resp_intersec.append(Person.objects.get(pk=resp).person_id)

    # 1. Сходство по общим признакам подписей

    sim_plus_hw, sim_minus_hw = jsm_gw(questionnaire_id, sim_signs)
    # print(sim_plus_hw)

    # 2. Сходства по опросникам

    filtered_cpcplus_q, filtered_cpcminus_q = find_similarities_q(resp_intersec_plus, resp_intersec_minus,
                                                                  resps_minimum, questionnaire_id,
                                                                  partitioning_scale_name_id)
    ocplus = []
    for f in filtered_cpcplus_q:
        ocplus.append([f[0], f[1]])
    ocminus = []
    for f in filtered_cpcminus_q:
        ocminus.append([f[0], f[1]])

    sim_plus_q = sort_hypothesis(ocplus)
    sim_minus_q = sort_hypothesis(ocminus)

    context = {
        'title': 'Сходства по опросникам и подписям',
        'questionnaires': questionnaires,
        'partitionings': partitionings,
        'questionnaire_id': int(questionnaire_id),
        'partitioning_scale_name_id': int(
            partitioning_scale_name_id),
        'resps_minimum': resps_minimum,
        'div_param': int(div_param),
        'sim_plus_hw': sim_plus_hw,
        'sim_minus_hw': sim_minus_hw,
        'sim_plus_q': sim_plus_q,
        'sim_minus_q': sim_minus_q,
        'quantity_plus_q': len(sim_plus_q),
        'quantity_minus_q': len(sim_minus_q),
        'quantity_plus_hw': len(sim_plus_hw),
        'quantity_minus_hw': len(sim_minus_hw),
        'is_gen_elem': True if sim_signs == 'gen_elem' else False,
        'div_plus': div_plus,
        'div_minus': div_minus
    }

    return render(request, 'research/similarities_general.html', context=context)


# список используемых респондентов (их id на сервере)
respondent_ids = [1, 2, 3, 5, 7, 9, 11, 66, 68, 20, 27, 13, 19, 67, 12, 16, 22]
# респонденты, у которых заполнен ОСТ
respondents_ost = [1, 2, 3, 5, 7, 9, 11, 12, 13, 16, 19, 20, 22, 27, 32, 35, 36, 37, 46, 65, 66, 67, 68]

def find_similarities_gw_potential(resp_list_plus, resp_list_minus):
    oplus = []
    for s in HwSet.objects.filter(verity=1, person__person_id__in=resp_list_plus).order_by(
            'person__person_id'):
        gw = HwGeneralWriting.objects.filter(set=s)
        pw = HwPartialWriting.objects.filter(first_letter_transcription__set=s)
        gv = set()

        # общие признаки подписи
        for g in gw:
            feature = g.general_sign_value.jsm_number
            if feature not in ['1;1', '2;2', '9;1', '11;1', '12;1'] and feature:
                # if feature in ['5;2', '5;3']:
                #     gv.add('5;2')
                # elif feature in ['3;1', '3;3']:
                #     gv.add('3;1')
                # elif feature in ['4;1', '4;3']:
                #     gv.add('4;1')
                # else:
                gv.add(g.general_sign_value.get_general_value())
                # print('h2: ', g.general_sign_value.jsm_number)

        # обобщенные частные признаки подписи
        for p in pw:
            if p.partial_sign.general_jsm_number:
                if p.partial_sign.partial_sign_value.partial_sign_type.partial_sign_type_number == 1:
                    gv.add(p.get_partial_value())
                    #gv.add(
                        #p.partial_sign.partial_sign_value.partial_sign_type.partial_sign_type_number + ';' +
                        #p.partial_sign.partial_sign_value.partial_sign_value_number + ';')
                else:
                    #gv.add(p.partial_sign.general_jsm_number)
                    gv.add(p.partial_sign.get_gen_partial_value())
        # for p in pw:
        # if p.partial_sign.opt_general_jsm_number:
        # gv.add(p.partial_sign.opt_general_jsm_number)
        # print(p.partial_sign.opt_general_jsm_number)
        oplus.append([gv, s.person])

    ominus = []
    for s in HwSet.objects.filter(verity=1, person__person_id__in=resp_list_minus).order_by(
            'person__person_id'):
        gw = HwGeneralWriting.objects.filter(set=s)
        pw = HwPartialWriting.objects.filter(first_letter_transcription__set=s)
        gv = set()

        # общие признаки подписи
        for g in gw:
            feature = g.general_sign_value.jsm_number
            if feature not in ['1;1', '2;2', '9;1', '11;1', '12;1'] and feature:
                # if feature in ['5;2', '5;3']:
                #     gv.add('5;2')
                # elif feature in ['3;1', '3;3']:
                #     gv.add('3;1')
                # elif feature in ['4;1', '4;3']:
                #     gv.add('4;1')
                # else:
                gv.add(g.general_sign_value.get_general_value())
                # print('h2: ', g.general_sign_value.jsm_number)

        # обобщенные частные признаки подписи
        for p in pw:
            if p.partial_sign.general_jsm_number:
                if p.partial_sign.partial_sign_value.partial_sign_type.partial_sign_type_number == 1:
                    gv.add(p.get_partial_value())
                else:
                    gv.add(p.partial_sign.get_gen_partial_value())
        # for p in pw:
        # if p.partial_sign.opt_general_jsm_number:
        # gv.add(p.partial_sign.opt_general_jsm_number)
        # print(p.partial_sign.opt_general_jsm_number)
        ominus.append([gv, s.person])

    # формируем список [[{мн-во неинф призн для человека}, чел], [], ...     ]

    # находим сходства
    cpcplus = jsm_create_similarities(oplus)
    cpcminus = jsm_create_similarities(ominus)
    # Фильтруем [ [{'',''},{pp}], [] ]
    filtered_cpcplus = jsm_filtration(cpcplus)
    filtered_cpcminus = jsm_filtration(cpcminus)
    # формируем список с объектами вместо номеров
    # ocplus = []
    # for f in filtered_cpcplus:
    #     obj = HwGeneralSignValue.objects.filter(jsm_number__in=f[0]).order_by(
    #         'general_sign_type__general_sign_type_number')
    #     ocplus.append([obj, f[1]])
    return filtered_cpcplus, filtered_cpcminus


def find_similarities_sign_groups(resp_list_plus, resp_list_minus):
    oplus = []
    for s in HwSet.objects.filter(verity=1, person__person_id__in=resp_list_plus).order_by(
            'person__person_id'):
        gw = HwGeneralWriting.objects.filter(set=s)
        pw = HwPartialWriting.objects.filter(first_letter_transcription__set=s)
        gv = set()

        # общие признаки подписи
        for g in gw:
            feature = g.general_sign_value.jsm_number
            type = g.general_sign_value.general_sign_type.general_sign_type_number
            if type in [3, 4, 5, 13, 14] and feature:
                gv.add(g.general_sign_value.get_general_value())

        # частные признаки подписи
        for p in pw:
            if p.partial_sign:
                gv.add(p.partial_sign.get_partial_value())

        oplus.append([gv, s.person])

    ominus = []
    for s in HwSet.objects.filter(verity=1, person__person_id__in=resp_list_minus).order_by(
            'person__person_id'):
        gw = HwGeneralWriting.objects.filter(set=s)
        pw = HwPartialWriting.objects.filter(first_letter_transcription__set=s)
        gv = set()

        # общие признаки подписи
        for g in gw:
            feature = g.general_sign_value.jsm_number
            type = g.general_sign_value.general_sign_type.general_sign_type_number
            if type in [3, 4, 5, 13, 14] and feature:
                gv.add(g.general_sign_value.get_general_value())

        # частные признаки подписи
        for p in pw:
            if p.partial_sign:
                gv.add(p.partial_sign.get_partial_value())

        ominus.append([gv, s.person])

    # находим сходства
    cpcplus = jsm_create_similarities(oplus)
    cpcminus = jsm_create_similarities(ominus)

    filtered_cpcplus = jsm_filtration(cpcplus)
    filtered_cpcminus = jsm_filtration(cpcminus)

    return filtered_cpcplus, filtered_cpcminus


def find_similarities_q(resp_list_plus, resp_list_minus, resps_minimum, questionnaire_id, partitioning_scale_name_id):
    sc = QQuestionScale.objects.filter(question__questionnaire=questionnaire_id).values('scale_id').exclude(scale_id=24)
    scales = QScale.objects.filter(scale_id__in=sc)

    assessments = get_assessments_by_questionnaire(questionnaire_id,
                                                   partitioning_scale_name_id,
                                                   scales)

    oplus = []
    # берем результаты по нужным нам шкалам
    results_plus = QResult.objects.filter(scale__in=scales, person__in=resp_list_plus).order_by('person')
    # results_plus = QResult.objects.filter(scale__in=scales, person__in=resp_list, person__sex='ж').order_by('person')
    # respondents = []
    for person_id in results_plus.values_list('person_id', flat=True).distinct():
        extensional = set()
        person_name = ''
        for result in results_plus.filter(person_id=person_id):
            assessment = count_assessment(assessments, questionnaire_id, result)
            ass = assessment.partitioning_scale.interval_set.interval_name.interval_name
            ass_scale = str(result.scale) + ': ' + str(ass)
            extensional.add(ass_scale)
            person_name = result.person
        # respondents.append(Person.objects.get(person_id=person_id))
        oplus.append([extensional, person_name])

    ominus = []
    results_minus = QResult.objects.filter(scale__in=scales, person__in=resp_list_minus).order_by('person')
    for person_id in results_minus.values_list('person_id', flat=True).distinct():
        extensional = set()
        person_name = ''
        for result in results_minus.filter(person_id=person_id):
            assessment = count_assessment(assessments, questionnaire_id, result)
            ass = assessment.partitioning_scale.interval_set.interval_name.interval_name
            ass_scale = str(result.scale) + ': ' + str(ass)
            extensional.add(ass_scale)
            person_name = result.person
        ominus.append([extensional, person_name])

    # находим сходства
    cpcplus = jsm_create_similarities(oplus)
    cpcminus = jsm_create_similarities(ominus)
    print(cpcplus[1])

    # Фильтруем
    filtered_cpcplus = jsm_filtration(cpcplus, ext_minimum=resps_minimum)
    filtered_cpcminus = jsm_filtration(cpcminus, ext_minimum=resps_minimum)
    print(filtered_cpcminus[1])

    # формируем список с объектами вместо номеров
    # ocplus = []
    # for f in filtered_cpcplus:
    #     #     print('f: ', f)
    #     # obj = QPartitioning.objects.filter(
    #     #     partitioning_id__in=f[0]).values_list('partitioning_scale__interval_set__interval_name__interval_name')
    #     #     # person = Person.objects.get(person_id__in=f[1])
    #     #     print(obj)
    #     ocplus.append([f[0], f[1]])
    # ocminus = []
    # for f in filtered_cpcminus:
    #     ocminus.append([f[0], f[1]])
    return filtered_cpcplus, filtered_cpcminus
from django.http import HttpResponse
from django.shortcuts import render

import pandas as pd
from pandas import ExcelWriter
import io

from main.models import QQuestionnaire, QPartitioningScaleName, \
    HwGeneralWriting
from django.core.cache import cache
from main.views.decorators import allowed_users
from django.contrib.auth.decorators import login_required

from main.domain.service_functions import divide_by_sex, \
    count_persons_w_tests_n_hw
from main.domain.views_functions import similarities_q_func, \
    similarities_hw_func, get_div_params, create_potential_hypotheses, classify_potential_hypotheses, \
    classify_potential_hypotheses_2_qstnrs


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def similarities_q_view(request):
    questionnaire_id = request.GET.get('select_questionnaire', 1)
    partitioning_scale_name_id = request.GET.get('select_partitioning', 2)
    resps_minimum = int(request.GET.get('resps_minimum', 2))
    q_or_hw = 'q'
    split_func = divide_by_sex

    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()

    sorted_similarities_plus, sorted_similarities_minus = similarities_q_func(questionnaire_id,
                                                                              partitioning_scale_name_id,
                                                                              resps_minimum,
                                                                              split_func, q_or_hw=q_or_hw)

    context = {
        'sim_plus': sorted_similarities_plus,
        'sim_minus': sorted_similarities_minus,
        'questionnaires': questionnaires,
        'partitionings': partitionings,
        'questionnaire_id': int(questionnaire_id),
        'partitioning_scale_name_id': int(partitioning_scale_name_id),
        'resps_minimum': int(resps_minimum),
        'quantity_plus': len(sorted_similarities_plus),
        'quantity_minus': len(sorted_similarities_minus),
        'title': 'Сходства по опросникам'
    }
    return render(request, 'research/similarities_q.html', context=context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def similarities_gw_view(request):

    resps_minimum = int(request.GET.get('resps_minimum', 2))
    div_option = int(request.GET.get('div_param', 1))
    sim_signs = request.GET.get('sim_signs', 'groups')
    verity = int(request.GET.get('verity', 1))

    scale_id = 11
    div_value = 3

    split_func, div_plus, div_minus, div_params = get_div_params(div_option,
                                                                 scale_id,
                                                                 div_value)

    resps_list = HwGeneralWriting.objects.prefetch_related('set').filter().values_list('set__person', flat=True).distinct()

    if cache.get(f'{resps_minimum}_{sim_signs}_{split_func}_{verity}'):
        sorted_similarities_plus, sorted_similarities_minus = cache.get(f'{resps_minimum}_{sim_signs}_{split_func}_{verity}')
    else:
        sorted_similarities_plus, sorted_similarities_minus = similarities_hw_func(resps_list=resps_list,
                                                                                   resps_minimum=resps_minimum,
                                                                                   sim_signs=sim_signs,
                                                                                   split_func=split_func,
                                                                                   q_or_hw='hw',
                                                                                   verity=verity)
        cache.set(f'{resps_minimum}_{sim_signs}_{split_func}_{verity}', [sorted_similarities_plus, sorted_similarities_minus])

    # вид списка такой: [[{},{}],[{},{}],[{},{}],[{},{}],]

    df_plus = pd.DataFrame(columns=['Признаки подписи', 'Респонденты'], data=sorted_similarities_plus)

    df_plus['Респонденты'] = df_plus['Респонденты'].astype(str).str.replace('{|}', '', regex=True)
    df_plus['Признаки подписи'] = df_plus['Признаки подписи'].astype(str).str.replace('{|}', '', regex=True)

    df_minus = pd.DataFrame(columns=['Признаки подписи', 'Респонденты'], data=sorted_similarities_minus)

    df_minus['Респонденты'] = df_minus['Респонденты'].astype(str).str.replace('{|}', '', regex=True)
    df_minus['Признаки подписи'] = df_minus['Признаки подписи'].astype(str).str.replace('{|}', '', regex=True)

    # функция записи нескольких листов в файл
    def save_xls(list_dfs, sheets_list):
        buffer = io.BytesIO()
        writer = ExcelWriter(buffer, engine='xlsxwriter')

        for n, df in enumerate(list_dfs):
            df.to_excel(writer, '%s' % sheets_list[n], index=False)
            formatting = writer.book.add_format({'text_wrap': True, 'valign': 'top'})
            writer.sheets[sheets_list[n]].set_column(df.columns.get_loc('Респонденты'), df.columns.get_loc('Респонденты'), 20, formatting)
            writer.sheets[sheets_list[n]].set_column(df.columns.get_loc('Признаки подписи'), df.columns.get_loc('Признаки подписи'), 80, formatting)

        writer.save()
        buffer.seek(0)
        return buffer.read()

    df_list = [df_plus, df_minus]

    # список названий вкладок в excel
    excel_name_list = ['Плюс гипотезы', 'Минус гипотезы']

    if request.GET.get('to_excel') is not None:

        xls = save_xls(df_list, excel_name_list)
        response = HttpResponse(xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="false_hw_similarities.xlsx"'
        return response

    print('DataFrame is written successfully to Excel File.')

    context = {
        'sim_plus': sorted_similarities_plus,
        'sim_minus': sorted_similarities_minus,
        'resps_minimum': int(resps_minimum),
        'title': 'Сходства по подписям',
        'quantity_plus': len(sorted_similarities_plus),
        'quantity_minus': len(sorted_similarities_minus),
        'verity': verity
    }

    return render(request, 'research/similarities_gw.html', context=context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def similarities_general_view(request):
    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()

    questionnaire_id = int(request.GET.get('select_questionnaire', 1))
    partitioning_scale_name_id = int(request.GET.get('select_partitioning', 2))
    resps_minimum = int(request.GET.get('resps_minimum', 2))
    div_option = int(request.GET.get('div_param', 1))
    sim_signs = request.GET.get('sim_signs', 'groups')

    scale_id = 11
    div_value = 3

    split_func, div_plus, div_minus, div_params = get_div_params(div_option, scale_id, div_value)

    resps_list = count_persons_w_tests_n_hw(questionnaire_id)

    # QUESTIONNAIRES

    sorted_similarities_plus_q, sorted_similarities_minus_q = similarities_q_func(
        questionnaire_id,
        partitioning_scale_name_id,
        resps_minimum,
        split_func,
        resps_list,
        q_or_hw='q', **div_params)

    # HANDWRITINGS

    sorted_similarities_plus_hw, sorted_similarities_minus_hw = similarities_hw_func(
        resps_list,
        resps_minimum,
        sim_signs,
        split_func,
        q_or_hw='hw', **div_params)

    context = {
        'title': 'Сходства по опросникам и подписям',
        'questionnaires': questionnaires,
        'partitionings': partitionings,
        'questionnaire_id': questionnaire_id,
        'partitioning_scale_name_id': partitioning_scale_name_id,
        'resps_minimum': resps_minimum,
        'div_param': div_option,
        'sim_plus_hw': sorted_similarities_plus_hw,
        'sim_minus_hw': sorted_similarities_minus_hw,
        'sim_plus_q': sorted_similarities_plus_q,
        'sim_minus_q': sorted_similarities_minus_q,
        'quantity_plus_q': len(sorted_similarities_plus_q),
        'quantity_minus_q': len(sorted_similarities_minus_q),
        'quantity_plus_hw': len(sorted_similarities_plus_hw),
        'quantity_minus_hw': len(sorted_similarities_minus_hw),
        'is_gen_elem': True if sim_signs == 'gen_elem' else False,
        'div_plus': div_plus,
        'div_minus': div_minus
    }

    return render(request, 'research/similarities_general.html', context=context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def potential_hypotheses_view(request):
    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()
    questionnaire_id = int(request.GET.get('select_questionnaire', 1))
    partitioning_scale_name_id = int(request.GET.get('select_partitioning', 2))
    resps_minimum = int(request.GET.get('resps_minimum', 2))

    div_option = int(request.GET.get('div_param', 1))
    sim_signs = request.GET.get('sim_signs', 'groups')

    scale_id = 11
    div_value = 3

    split_func, div_plus, div_minus, div_params = get_div_params(div_option,
                                                                 scale_id,
                                                                 div_value)
    if cache.get(f'{questionnaire_id}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}'):
        potential_hypotheses_plus, potential_hypotheses_minus, *rest = cache.get(f'{questionnaire_id}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}')
    else:
        potential_hypotheses_plus, potential_hypotheses_minus, *rest = create_potential_hypotheses(
            questionnaire_id=questionnaire_id,
            partitioning_scale_name_id=partitioning_scale_name_id,
            sim_signs=sim_signs,
            resps_minimum=resps_minimum,
            split_func=split_func,
            **div_params,
        )
        cache.set(f'{questionnaire_id}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}',
                  [potential_hypotheses_plus, potential_hypotheses_minus, *rest])

    return render(request, 'research/potential_hypotheses.html',
                  {'potential_hypotheses_plus': potential_hypotheses_plus,
                   'potential_hypotheses_minus': potential_hypotheses_minus,
                   'questionnaires': questionnaires,
                   'partitionings': partitionings,
                   'questionnaire_id': questionnaire_id,
                   'partitioning_scale_name_id':
                       partitioning_scale_name_id,
                   'resps_minimum': resps_minimum,
                   'title': 'Потенциальные гипотезы',
                   'quantity_plus': len(potential_hypotheses_plus),
                   'quantity_minus': len(potential_hypotheses_minus),
                   'div_param': div_option,
                   'is_gen_elem': True if sim_signs == 'gen_elem' else False,
                   'div_plus': div_plus,
                   'div_minus': div_minus
                   })


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def jsm_choose_option_view(request):

    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()
    questionnaire_id = int(request.GET.get('select_questionnaire', 1))
    partitioning_scale_name_id = int(request.GET.get('select_partitioning', 2))
    resps_minimum = int(request.GET.get('resps_minimum', 2))

    div_option = int(request.GET.get('div_param', 1))
    sim_signs = request.GET.get('sim_signs', 'groups')

    scale_id = 11
    div_value = 3

    split_func, div_plus, div_minus, div_params = get_div_params(div_option,
                                                                 scale_id,
                                                                 div_value)

    if cache.get(f'{questionnaire_id}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}'):
        sorted_sim_all_plus, sorted_sim_all_minus, sorted_hypotheses_hw_plus, sorted_hypotheses_hw_minus, sorted_hypotheses_q_plus, sorted_hypotheses_q_minus = cache.get(
            f'{questionnaire_id}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}')
    else:
        sorted_sim_all_plus, sorted_sim_all_minus, sorted_hypotheses_hw_plus, sorted_hypotheses_hw_minus, sorted_hypotheses_q_plus, sorted_hypotheses_q_minus = create_potential_hypotheses(
            questionnaire_id=questionnaire_id,
            partitioning_scale_name_id=partitioning_scale_name_id,
            sim_signs=sim_signs,
            resps_minimum=resps_minimum,
            split_func=split_func,
            **div_params
        )
        cache.set(f'{questionnaire_id}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}',
                  [sorted_sim_all_plus, sorted_sim_all_minus, sorted_hypotheses_hw_plus, sorted_hypotheses_hw_minus,
                   sorted_hypotheses_q_plus, sorted_hypotheses_q_minus])

    direct_jsm_plus, inverse_jsm_plus, both_jsm_plus, neither_jsm_plus, direct_jsm_minus, \
    inverse_jsm_minus, both_jsm_minus, neither_jsm_minus = classify_potential_hypotheses(sorted_sim_all_plus,
                                                                                         sorted_sim_all_minus,
                                                                                         sorted_hypotheses_hw_plus,
                                                                                         sorted_hypotheses_hw_minus,
                                                                                         sorted_hypotheses_q_plus,
                                                                                         sorted_hypotheses_q_minus)

    quantity_plus = len(sorted_sim_all_plus)
    quantity_minus = len(sorted_sim_all_minus)
    context = {
        'questionnaires': questionnaires,
        'partitionings': partitionings,
        'questionnaire_id': questionnaire_id,
        'partitioning_scale_name_id': partitioning_scale_name_id,
        'resps_minimum': resps_minimum,
        'div_param': div_option,
        'quantity_plus': quantity_plus,
        'quantity_minus': quantity_minus,
        'direct_hypotheses_plus': direct_jsm_plus,
        'direct_quantity_plus': len(direct_jsm_plus),
        'inverse_hypotheses_plus': inverse_jsm_plus,
        'inverse_quantity_plus': len(inverse_jsm_plus),
        'both_hypotheses_plus': both_jsm_plus,
        'both_quantity_plus': len(both_jsm_plus),
        'neither_hypotheses_plus': neither_jsm_plus,
        'neither_quantity_plus': len(neither_jsm_plus),
        'direct_hypotheses_minus': direct_jsm_minus,
        'direct_quantity_minus': len(direct_jsm_minus),
        'inverse_hypotheses_minus': inverse_jsm_minus,
        'inverse_quantity_minus': len(inverse_jsm_minus),
        'both_hypotheses_minus': both_jsm_minus,
        'both_quantity_minus': len(both_jsm_minus),
        'neither_hypotheses_minus': neither_jsm_minus,
        'neither_quantity_minus': len(neither_jsm_minus),
        'is_gen_elem': True if sim_signs == 'gen_elem' else False,
        'title': 'Выбор метода',
        'div_plus': div_plus,
        'div_minus': div_minus
    }
    return render(request, 'research/jsm_choose_option.html', context=context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def jsm_intersect_qstnrs(request):
    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()
    questionnaire_id_1 = int(request.GET.get('select_questionnaire', 1))
    questionnaire_id_2 = int(request.GET.get('select_questionnaire_2', 2))
    partitioning_scale_name_id = int(request.GET.get('select_partitioning', 2))
    resps_minimum = int(request.GET.get('resps_minimum', 2))

    div_option = int(request.GET.get('div_param', 1))
    sim_signs = request.GET.get('sim_signs', 'groups')

    scale_id = 11
    div_value = 3

    split_func, div_plus, div_minus, div_params = get_div_params(div_option,
                                                                 scale_id,
                                                                 div_value)

    if cache.get(f'{questionnaire_id_1}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}'):
        sorted_sim_all_plus_1, sorted_sim_all_minus_1, sorted_hypotheses_hw_plus_1, sorted_hypotheses_hw_minus_1, sorted_hypotheses_q_plus_1, sorted_hypotheses_q_minus_1 = cache.get(
            f'{questionnaire_id_1}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}')
    else:
        sorted_sim_all_plus_1, sorted_sim_all_minus_1, sorted_hypotheses_hw_plus_1, sorted_hypotheses_hw_minus_1, sorted_hypotheses_q_plus_1, sorted_hypotheses_q_minus_1 = create_potential_hypotheses(
            questionnaire_id=questionnaire_id_1,
            partitioning_scale_name_id=partitioning_scale_name_id,
            sim_signs=sim_signs,
            resps_minimum=resps_minimum,
            split_func=split_func,
            **div_params
        )
        cache.set(f'{questionnaire_id_1}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}',
                  [sorted_sim_all_plus_1, sorted_sim_all_minus_1, sorted_hypotheses_hw_plus_1, sorted_hypotheses_hw_minus_1,
                   sorted_hypotheses_q_plus_1, sorted_hypotheses_q_minus_1])

    if cache.get(f'{questionnaire_id_2}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}'):
        sorted_sim_all_plus_2, sorted_sim_all_minus_2, sorted_hypotheses_hw_plus_2, sorted_hypotheses_hw_minus_2, sorted_hypotheses_q_plus_2, sorted_hypotheses_q_minus_2 = cache.get(
            f'{questionnaire_id_2}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}')
    else:
        sorted_sim_all_plus_2, sorted_sim_all_minus_2, sorted_hypotheses_hw_plus_2, sorted_hypotheses_hw_minus_2, sorted_hypotheses_q_plus_2, sorted_hypotheses_q_minus_2 = create_potential_hypotheses(
            questionnaire_id=questionnaire_id_2,
            partitioning_scale_name_id=partitioning_scale_name_id,
            sim_signs=sim_signs,
            resps_minimum=resps_minimum,
            split_func=split_func,
            **div_params
        )
        cache.set(f'{questionnaire_id_2}_{partitioning_scale_name_id}_{resps_minimum}_{div_option}_{sim_signs}',
                  [sorted_sim_all_plus_2, sorted_sim_all_minus_2, sorted_hypotheses_hw_plus_2, sorted_hypotheses_hw_minus_2,
                   sorted_hypotheses_q_plus_2, sorted_hypotheses_q_minus_2])

    res_lst_plus = []
    res_lst_minus = []

    for pers_set_1 in sorted_sim_all_plus_1:
        for pers_set_2 in sorted_sim_all_plus_2:
            if pers_set_1[0] == pers_set_2[0]:
                res_lst_plus.append([pers_set_1[0], pers_set_1[1], pers_set_2[1], pers_set_1[2]])

    for pers_set_1 in sorted_sim_all_minus_1:
        for pers_set_2 in sorted_sim_all_minus_2:
            if pers_set_1[0] == pers_set_2[0]:
                res_lst_minus.append([pers_set_1[0], pers_set_1[1], pers_set_2[1], pers_set_1[2]])

    classify_potential_hypotheses_2_qstnrs(res_lst_plus,
                                           res_lst_minus,
                                           sorted_hypotheses_hw_plus_1,
                                           sorted_hypotheses_hw_minus_1,
                                           sorted_hypotheses_q_plus_1,
                                           sorted_hypotheses_q_minus_1,
                                           sorted_hypotheses_q_plus_2,
                                           sorted_hypotheses_q_minus_2
                                           )

    df_plus = pd.DataFrame(columns=['Респонденты', 'Опросник 1', 'Опросник 2', 'Признаки подписи', 'Исчерпываемость'],
                               data=res_lst_plus)
    df_minus = pd.DataFrame(columns=['Респонденты', 'Опросник 1', 'Опросник 2', 'Признаки подписи', 'Исчерпываемость'],
                            data=res_lst_minus)

    for col in df_plus.columns:
        df_plus[col] = df_plus[col].astype(str).str.replace('{|}', '', regex=True)
    for col in df_minus.columns:
        df_minus[col] = df_minus[col].astype(str).str.replace('{|}', '', regex=True)

    # функция записи нескольких листов в файл
    def save_xls(list_dfs, sheets_list):
        buffer = io.BytesIO()
        writer = ExcelWriter(buffer, engine='xlsxwriter')

        for n, df in enumerate(list_dfs):
            df.to_excel(writer, '%s' % sheets_list[n], index=False)
            formatting = writer.book.add_format({'text_wrap': True, 'valign': 'top'})
            writer.sheets[sheets_list[n]].set_column(df.columns.get_loc('Респонденты'), df.columns.get_loc('Респонденты'), 20, formatting)
            writer.sheets[sheets_list[n]].set_column(df.columns.get_loc('Опросник 1'), df.columns.get_loc('Опросник 1'), 30, formatting)
            writer.sheets[sheets_list[n]].set_column(df.columns.get_loc('Опросник 2'), df.columns.get_loc('Опросник 2'), 30, formatting)
            writer.sheets[sheets_list[n]].set_column(df.columns.get_loc('Признаки подписи'), df.columns.get_loc('Признаки подписи'), 80, formatting)
            writer.sheets[sheets_list[n]].set_column(df.columns.get_loc('Исчерпываемость'), df.columns.get_loc('Исчерпываемость'), 30, formatting)
        print(df)
        writer.save()
        buffer.seek(0)

        return buffer.read()

    df_list = [df_plus, df_minus]

    # список названий вкладок в excel
    excel_name_list = ['Плюс гипотезы', 'Минус гипотезы']

    if request.GET.get('to_excel') is not None:

        xls = save_xls(df_list, excel_name_list)
        response = HttpResponse(xls, content_type='application/vnd.ms-excel')
        # нельзя называть файл кириллицей, иначе ломается, видимо из-за кодировки utf-8
        response['Content-Disposition'] = 'attachment; filename="questionnaires_intersection.xlsx"'
        return response

    context = {
        'title': 'Сравнение опросников',
        'questionnaires': questionnaires,
        'partitionings': partitionings,
        'questionnaire_id_1': questionnaire_id_1,
        'questionnaire_id_2': questionnaire_id_2,
        'partitioning_scale_name_id': partitioning_scale_name_id,
        'resps_minimum': resps_minimum,
        'div_param': div_option,
        'div_plus': div_plus,
        'div_minus': div_minus,
        'res_lst_plus': res_lst_plus,
        'res_lst_minus': res_lst_minus,
        'quantity_plus': len(res_lst_plus),
        'quantity_minus': len(res_lst_minus),

    }

    return render(request, 'research/jsm_intersect_qstnrs.html', context=context)